package Queues;

/**
 * Created by Alexander on 13-11-2015.
 */
public class IntQueue {
    private final int PLADSER = 10;
    private int item = 0;
    private int start = -1;
    private int slut = -1;
    public  int[] intArray = new int[PLADSER];


    public boolean isFull(){
        if (item == PLADSER){
            return true;
        }else{
            return false;
        }
    }

    private void removeFromFront() {
        intArray[start+1]  = item;
    }

    public void add(int i){
        if (isFull()){
            removeFromFront();
        }else {
            item++;
            slut++;
            intArray[slut] = i;
        }
        }
    public int[] getIntArray(){
        return intArray;
    }
}
