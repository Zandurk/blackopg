package uge47;


import java.util.*;

/**
 * Created by Alexander on 27-11-2015.
 */
public class main {
private static boolean check = false;
private static  ArrayList<String> stringArrayList = new ArrayList<>();
private static HashMap<String, String> opgave13Map = new HashMap<>();
private static HashMap<String, Integer> opgave14Map1 = new HashMap<>();
private static HashMap<String, Integer> opgave14Map2 = new HashMap<>();


    private static int counter = 0;


    public static void main(String[] args) {
        opgave14Map1.put("Tob",1);
        opgave14Map1.put("James",2);

        opgave14Map2.put("Tob", 1);
        opgave14Map2.put("James", 2);

        opgave12(stringArrayList);
        opgave13Map.put("1", "s");
        opgave13Map.put("s", "tg");
        opgave13Map.put("Wollah", "s");
        System.out.println(opgave13(opgave13Map));
       System.out.println(opgave14(opgave14Map1, opgave14Map2));
        ArrayList<Integer> integerArrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integerArrayList.add(i);
        }
        integerArrayList.add(9);
        integerArrayList.add(9);
        integerArrayList.add(2);
        integerArrayList.add(2);
        integerArrayList.add(2);

        System.out.println(opgave15(integerArrayList));
    }

    private static String opgave15(ArrayList<Integer> integerArrayList) {
        HashMap<Integer,Integer> intmap = new HashMap<>();
        int a = 0;
        int b = 0;
        int s = 0;
        for (int i = 0; i <integerArrayList.size(); i++) {
            if(intmap.get(integerArrayList.get(i)) == null){
                intmap.put(integerArrayList.get(i),integerArrayList.get(1));
            }else {
              int q =  intmap.get(integerArrayList.get(i).intValue());
                q++;
                intmap.put(integerArrayList.get(i),q);
            }

            int c = Collections.max(intmap.values());
            for(Map.Entry<Integer,Integer> entry : intmap.entrySet()){
                    if (entry.getValue().equals(c)){
                        a = entry.getKey();
                        b = entry.getValue();
                    }
            }
        }

        return a + " " + b;
    }

    private static HashMap<String,Integer> opgave14(HashMap<String, Integer> opgave14Map1, HashMap<String, Integer> opgave14Map2) {
        HashMap<String, Integer> finalMap = new HashMap<>();
        for (HashMap.Entry<String, Integer> entry: opgave14Map1.entrySet()) {
            if (opgave14Map2.get(entry.getKey()).equals(opgave14Map1.get(entry.getKey()))){
                    finalMap.put(entry.getKey(),entry.getValue());
            }
        }
            return finalMap;
    }

    private static boolean opgave13(HashMap<String, String> opgave13Map) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        boolean checker = false;
        int j = 0;
        for (HashMap.Entry<String, String> entry: opgave13Map.entrySet()) {
            stringArrayList.add(entry.getValue());
        }
        for (int i = 0; i < stringArrayList.size(); i++) {
            if (stringArrayList.get(i).equals(stringArrayList.get(j + 1))){
                checker = false;
            } else {
                checker = true;
            }
        }

        return checker;
    }

    private static boolean opgave12(ArrayList<String> stringArrayList) {
        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i < stringArrayList.size(); i++) {
            String temp = stringArrayList.get(i);
            if (map.get(temp) == null) {
                map.put(temp, 1);
            } else {
            int v = map.get(temp).intValue();
                v++;
                map.put(temp,v);
            }
        }
            for (HashMap.Entry<String, Integer> entry : map.entrySet()){
                System.out.println(entry.getKey()+ entry.getValue());
                if (entry.getValue() ==3){
                    check = true;
                }
            }
        return check;
    }
}
