package uge35;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Created by Alex on 30-08-2015.
 */



public class lommeregnermain extends Application{

    public Stage primaryStage;
    private String output1;
    private String output;
    public TextField result;
    private List<String> buttons = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9");
    private List<String> buttonsCalc = Arrays.asList("+", "-", "*", "/");
    private String resultat;

    //S� lommeregneren kan k�re p� �ldre maskiner
    public static void main(String[] args) {
        Application.launch(args);
    }

    //Prim�re vindue
    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane bPane = new BorderPane();
        //Definerer de forskellige dele af borderpanen i metoder
        HBox hbox = hBoxsettings();
        TilePane tp = tpSettings();
        TilePane tp2 = tpSettings2();
        bPane.setTop(result);
        bPane.setCenter(tp);
        bPane.setRight(tp2);
        bPane.setStyle("-fx-background: #d3d3d3;");


        Scene scene = new Scene(bPane, 220, 170);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Lommeregner");
        primaryStage.setResizable(false);
        primaryStage.show();

    }


    private HBox hBoxsettings() {
        HBox hbox = new HBox();
        hbox.setPrefSize(220, 60);
        hbox.setSpacing(10);
        //Textfield til at vise resultat
        result = new TextField();
        result.setPrefSize(240, 60);
        result.setEditable(false);
        hbox.getChildren().addAll(result);

        return hbox;
    }

    private TilePane tpSettings() {
        TilePane tp = new TilePane();
        tp.setHgap(5);
        tp.setVgap(5);
        tp.setPrefColumns(4);

        //Tal-knapperne
        for (String button : buttons) {
            Button b = new Button(button);
            b.setPrefSize(35, 35);
            tp.getChildren().add(b);
            b.setOnAction((e) -> result.appendText(b.getText()));
        }


        return tp;
    }

    private TilePane tpSettings2() {
        TilePane tp2 = new TilePane();
        tp2.setHgap(5);
        tp2.setVgap(5);
        tp2.setPrefColumns(0);
        //Operator-knapperne
        for (String buttonCalc : buttonsCalc) {
            Button b = new Button(buttonCalc);
            b.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            tp2.getChildren().add(b);
            b.setOnAction((e) -> result.appendText(b.getText()));
        }

        //= knappen samler rengestykket og kalder calculate for at f� resultat.
        Button b14 = new Button(" =");
        b14.setOnAction(event -> {
            result.appendText(b14.getText());
            output = result.getText();
            output1 = output;
            //fjerne = da den skaber problemer i rengestykket
            output = output1.replaceAll("=", "");
            calculate(output);
        });
        //Clear
        Button b15 = new Button("C");
        b15.setOnAction(e -> result.setText(""));
        tp2.getChildren().addAll(b14, b15);
        tp2.setPrefSize(70, 70);


        return tp2;
    }



    private void calculate(String output) {
        //En del printlines, s� man kan f�lge det der sker n�r man trykker p� knapperne

        double forsteDel = 0;
        double andenDel = 0;
        String[] forsog;
        System.out.println(output);

        //if-statement splitter String output ved operatoren, dette er ikke optimeret til flere regnestykker endnu!
        try {
            if (output.contains("+")) {
                forsog = output.split(Pattern.quote("+"));
                forsteDel = Double.valueOf(forsog[0]);
                andenDel = Double.valueOf(forsog[1]);
                System.out.println(forsog[0]);
                System.out.println(forsog[1]);

                resultat = Double.toString(forsteDel + andenDel);

            } else if (output.contains("-")) {
                forsog = output.split(Pattern.quote("-"));
                forsteDel = Double.valueOf(forsog[0]);
                andenDel = Double.valueOf(forsog[1]);
                System.out.println(forsog[0]);
                System.out.println(forsog[1]);

                resultat = Double.toString(forsteDel - andenDel);

            } else if (output.contains("*")) {
                forsog = output.split(Pattern.quote("*"));
                forsteDel = Double.valueOf(forsog[0]);
                andenDel = Double.valueOf(forsog[1]);
                System.out.println(forsog[0]);
                System.out.println(forsog[1]);

                resultat = Double.toString(forsteDel * andenDel);

            } else if (output.contains("/")) {
                forsog = output.split(Pattern.quote("/"));
                forsteDel = Double.valueOf(forsog[0]);
                andenDel = Double.valueOf(forsog[1]);
                System.out.println(forsteDel);
                System.out.println(andenDel);

                resultat = Double.toString(forsteDel / andenDel);
            }

        // I stedet for at skulle genstarte, pop-up der forklarer fejlen
        } catch (NumberFormatException e){
            final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            VBox dialogVbox = new VBox(20);
            dialogVbox.getChildren().add(new Text("Lige pt kan lommeregneren ikke regne med flere operatorere p� en gang"));
            Scene dialogScene = new Scene(dialogVbox, 400, 50);
            dialog.setScene(dialogScene);
            dialog.setTitle("Fejl: Flere operatorer");
            dialog.show();
        }
        //Viser resultat
        result.setText("");
        result.appendText(" "+resultat);


    }



}
