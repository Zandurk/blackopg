package uge36.sample;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.PopupBuilder;
import javafx.stage.Stage;

public class Controller {

private Stage primaryStage = new Stage();


    public MenuItem et;
    public MenuItem to;
    public MenuItem tre;
    public MenuItem fire;
    public MenuItem fem;
    public MenuItem skes;
    public MenuItem luk;
    public Main m;

@FXML
    public void luk(ActionEvent event){
         System.exit(0);
    }

    @FXML
    public void andet(ActionEvent event){
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        dialog.setTitle("Fejl: Demomodel");
        VBox dialogVbox = new VBox(20);
        dialogVbox.getChildren().add(new Text("Denne applikation er en demomodel og har derfor ikke funktioner endnu."));
        Scene dialogScene = new Scene(dialogVbox, 400, 70);
        dialog.setScene(dialogScene);
        dialog.show();
    }
}
