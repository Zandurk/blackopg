package uge48;

/**
 * Created by Alex on 30-11-2015.
 */
public class main {

    public static void main(String[] args) {
        //Exercises 14.1-14.2
        /*
        14.1
        Give the adjacency matrix and ajencency list for
        a. the weighted graph in Figure 14-33.
        b. the directed graph in Figure 14-34.
        */
        exercise14_1();

        /*
        * Show that the adjacency list in Figure 14-8b require less
        * memory than the adjacency matrix in Figure 14-6b.
        */
        exercise14_2();
    }


    private static void exercise14_1() {
    /*
    a.
     0 = 10 , 2
     1 = 10, 5, 7
     4 = 2, 5, 6
     5 = 6, 3
     2 = 7, 3, 4, 1
     3 = 4
     6 = 1

         ***********************
        *   0 1 4 2 5 3 6 *
        * 0 0 1 1 0 0 0 0   *
        * 1 1 0 1 1 0 0 0   *
        * 4 1 1 0 0 1 0 0   *
        * 2 0 1 0 0 1 1 1   *
        * 5 0 0 1 1 0 0 0   *
        * 3 0 0 0 1 0 0 0   *
        * 6 0 0 0 1 0 0 0   *
        * ---------------------

    b.
    a -> c
    c -> e
    e -> g
    g -> c
    c -> d
    d -> h
    h -> g
    b -> a, d, h
    f -> g, i
    i -> c



            a b c d e f g h i
          a 0 0 1 0 0 0 0 0 0
          b 1 0 0 1 0 0 0 1 0
          c 0 0 0 1 1 0 0 0 0
          d 0 0 0 0 0 0 0 1 0
          e 0 0 0 0 0 0 1 0 0
          f 0 0 0 0 0 0 1 0 1
          g 0 0 1 0 0 0 0 0 0
          h 0 0 0 0 0 0 0 0 1
          i 0 0 1 0 0 0 0 0 0
        -------------------------

    */
    }


    private static void exercise14_2() {
        /*
        *
        * Som man kan se kr�ver listen 14-8B mindre ram, da den har linket
        * noderne sammen, s� hvis man skal finde en node beh�ver man ikke at
        * s�ge alle noderne igennem.
        * Det skal man i 14-6b, da den ikke kan se den direkte forbindelse til en bestemt
        * node.
        *
        */




    }



}
