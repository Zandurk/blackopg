package Stacks_Sorting;

import java.util.ArrayList;

/**
 * Created by Alexander on 15-11-2015.
 */
public class MyStack {
    int stackPointer = 0;
    int counter = 0;
    private ArrayList<String> stackArray = new ArrayList<>();

    public void push(String i){
        stackPointer++;
        stackArray.add(i);
    }

    public String pop(){
        String temp = String.valueOf(stackArray.indexOf(stackPointer));
        stackArray.remove(stackPointer);
        stackPointer--;
        return temp;
    }

    public String top(){
        return String.valueOf(stackArray.indexOf(stackPointer));
    }

    public int size(){
        return stackPointer;
    }
    public boolean isEmpty(){
        if (stackPointer == 0)
        {
            return true;
        }else {
            return false;
        }
    }

    private void remove(int i){
        stackArray.remove(i);


    }

    public ArrayList<String> trim(ArrayList<String> as, String ms){
        char c = '<';
        for (int i = ms.length(); i > 0; i--) {
            push(ms.substring(i));
            if (as.indexOf(i)== c) {
                counter++;
                remove(i);
                stackPointer--;
         }
        }

        return as;
    }
    public ArrayList<String> getStackArray(){
        return stackArray;
    }


}
