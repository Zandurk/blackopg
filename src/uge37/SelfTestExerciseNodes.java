package uge37;

/**
 * Created by Alex on 19-09-2015.
 */
public class SelfTestExerciseNodes {
    public static void main(String[] args) {

    }

    class Node{
        int item;
        Node next;

        Node(int newItem){
            item = newItem;
            next = null;
            }

        Node(int newItem, Node nextNode){
            item = newItem;
            next = nextNode;
        }
    }

}


/*
a. p = new IntegerNode();
 Laver en ny instans af IntegerNode - alts� p bliver en IntegerNode
    Men der mangler ogs� parametre.

b. p = new IntegerNode(1, head);
Fodrer 1, som item og et head, som indikerer startpunktet p� LinkedListen.

c. p = new IntegerNode(1);
   q = new IntegerNode(3, p);
   p.next = head;
   head = q;

   p.next = head - her bliver linkedlisten sat til at 1 linked til 3.
   head = q - her bliver head s� �ndret umiddelbart efter til 3.

   S� 1 er sat sammen med 3 nu.

d.  x = 3;
    p = new IntegerNode(x, head.next);
    q = new IntegerNode(p);
    head = q;

    p = new IntegerNode(3, ?); her ved den ikke hvad head.next er, da hed ikke har nogen v�rdi endnu
    q = p;
    head = q; head f�r f�rst en v�rdi her, men listen bliver sat til 3 linked med q, som ikke har en tal v�rdi

e. IntegerNode curr = head;
    while(curr != null){
    curr.item++;
    curr = curr.next;
    }

curr = head betyder at vi tager den nuv�rende node og linker den til noden efter
 - det sker her : curr.item++
 da vi kalder curr.next efter for at g� videre til n�ste.

 f. x = 3;
    p = new IntegerNode(x, head.next);
    head = p;

p = new IntegerNode(3, head.next);
head = 3;

Hvis denne liste skal fungere skal den have en metode der linker de forskellige noder sammen
Ellers er det ikke en liste, men bare en enkelt liste.




* */