package uge37;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Alex on 19-09-2015.
 */
public class chapter10 {

    public static void main(String[] args) {

    }


    //10.1
    private int vowels(ArrayList<String> as){
    int count = 0;
        if (as.isEmpty()){
            return count;
        } else {
            char[] test = new char[]{'a', 'e', 'i', 'o', 'u'};

            for (int i = 0; i < as.size(); i++) {
                if (test[i] == as.get(i).charAt(i)) {
                    count++;
                }
            }
            return count;
        }
    }

    //10.2
    private ArrayList<String> swapPairs(ArrayList<String> as){
        ArrayList<String> sa = new ArrayList<>();
        for(int i = 0; i <as.size(); i++) {

            sa.add(as.get(i + 1));
        }

        as = sa;

        return as;
    }


    //10.3
    private ArrayList<String> removeOddLength(ArrayList<String> as){
        ArrayList<String> sa = new ArrayList<>();
        int a = 2;
        int b = 2;
        sa = as;
        for(int i = 0; i < as.size(); i++){

          if (sa.get(i).toString().length() != a*b || sa.get(i).length() != 2) {
              sa.remove(i);
          }
            b++;
        }

        as = sa;
        return as;
    }

    //10.4
    private ArrayList<String> quadString(ArrayList<String> as){
        ArrayList<String> sa = new ArrayList<>();
        sa = as;

        for (int i = 0; i < sa.size(); i++){
           for(int j = 0; j < 5; j++){
               as.add(j, sa.get(j));
           }
        }

        return as;
    }

    //10.5
    private ArrayList<Integer> scaledByK(ArrayList<Integer> ai){
        ArrayList<Integer> sa = new ArrayList<>();
        Random random = new Random();
        int k = random.nextInt();
        sa = ai;

        for(int i = 0; i < sa.size(); i++){
            for (int j = 0; j < k; j++){
            if(sa.get(i).toString().charAt(i) == k) {
                ai.add(sa.get(i));
            }
            }
        }



        return ai;
    }


    //10.6
    private ArrayList<Integer> maxToFront(ArrayList<Integer> ai){
        ArrayList<Integer> sa = new ArrayList<>();
        ai = sa;
        int tal;

        for(int i = 0; i < ai.size(); i++){
            tal = ai.get(i).compareTo(ai.get(i+1));
        if(tal > 0){
           sa.clear();
           sa.add(i);
          ai = sa;
        }

        }

        return ai;
    }


    //10.7
    private ArrayList<String> removeDuplicates(ArrayList<String> as) {
        ArrayList<String> sa = new ArrayList<>();
        sa = as;
        for (int i = 0; i < sa.size(); i++){
            if(sa.get(i).equals(sa.get(i+1))){
                sa.remove(i);
            }
        }
        as = sa;
        return as;
    }

    //10.8
    private ArrayList<Integer> removeOdd(ArrayList<Integer> ai){
      int a = 2;

        for (int i=0; i < ai.size(); i++){
            if(ai.get(i) != a){
                ai.remove(i);
            }
           a = a + 2;
        }

        return ai;
    }

    //10.12
    private ArrayList<String> mark4Length(ArrayList<String> as){
        ArrayList<String> sa = new ArrayList<>();
        sa = as;

        for (int i = 0; i < sa.size(); i++){
            if(sa.get(i).toString().length() == 4){
                sa.remove(i-1);
                sa.add(i-1, "****");
            }
        }
        as = sa;
        return as;
    }
}
