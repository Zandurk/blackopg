package uge41;

/**
 * Created by Alex on 30-11-2015.
 */
public class mainOpgaverFraBog {
    private static int sum = 0;

    public static void main(String[] args) {
        /*
        7.1. Consider the method displayRowOfIntegers that displays any given integer twice the specified number on one line.
        Implement using recursion.
        */
        displayRowOfInteger(56465, 9);
        System.out.println("");
        System.out.println("Antal gange besøgt: "+sum);

    }

    private static void displayRowOfInteger(int number, int numberOfTimes) {

        if (numberOfTimes == 1){
            System.out.print(number+" ");
            sum++;
         } else {
             System.out.print(number+" ");
            if (numberOfTimes != 0) {
                displayRowOfInteger(number, numberOfTimes-1);
              sum++;
            }
        }

    }
}
