package uge41;

import java.util.ArrayList;

/**
 * Created by Alexander on 15-11-2015.
 */
public class MyStack {
    int stackPointer = 0;
    int counter = 0;
    private ArrayList<String> stackArray = new ArrayList<>();

    public void push(String i){
        for (int j = 0; j < i.length(); j++) {
            stackPointer++;
            stackArray.add(String.valueOf(i.charAt(j)));
        }
        System.out.println(stackPointer);
    }

    public String pop(){
        String temp = String.valueOf(stackArray.indexOf(stackPointer));
        stackArray.remove(stackPointer);
        stackPointer--;
        return temp;
    }

    public String top(){
        return String.valueOf(stackArray.indexOf(stackPointer));
    }

    public int size(){
        return stackPointer;
    }

    public boolean isEmpty(){
        if (stackPointer == 0)
        {
            return true;
        }else {
            return false;
        }
    }

    private void remove(int i){
        stackArray.remove(i);


    }

    public ArrayList<String> getStackArray(){
        return stackArray;
    }

    public ArrayList<String> backWards(){
        ArrayList<String> tempStackArray = new ArrayList<>();
        if (!isEmpty()){
            for (int i = stackArray.size()-1; i >= 0; i--) {
                tempStackArray.add(stackArray.get(i));
            }
        }
        return tempStackArray;
    }

}
