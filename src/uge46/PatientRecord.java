package uge46;

/**
 * Created by Alexander on 01-12-2015.
 */
public class PatientRecord extends PatientDataBase {

    int id;
    String name;
    String reasonForVisit;
    String treatment;
    int date;

    public PatientRecord(int id, String name, String reasonForVisit, String treatment, int date) {
        this.id = id;
        this.name = name;
        this.reasonForVisit = reasonForVisit;
        this.treatment = treatment;
        this.date = date;
    }

    public String getTreatment() {
        return treatment;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getReasonForVisit() {
        return reasonForVisit;
    }

    public int getDate() {
        return date;
    }

}
