package uge46;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexander on 01-12-2015.
 */
public class PatientDataBase {
    private ArrayList<PatientRecord> patientRecords = new ArrayList<>();
    private HashMap<Integer, Integer> dateArray = new HashMap<>();
    private HashMap<Integer, String> reasonMap = new HashMap<>();
    private HashMap<Integer, String> treatmentMap = new HashMap<>();
    private HashMap<Integer, String> nameMap = new HashMap<>();



    private String getReason(int id){
        String reason = "";
            for (Map.Entry<Integer, String> entry: reasonMap.entrySet()){
                if (entry.getKey() == id){
                    reason = entry.getValue().toString();
                }
            }

        return reason;
        }

private String getTreatment(int id){
    String treatment = "";
    for (Map.Entry<Integer, String> entry: treatmentMap.entrySet()){
        if (entry.getKey() == id){
            treatment = entry.getValue().toString();
        }
    }

    return treatment;
}


    private ArrayList<Integer> getDates(int id){
        ArrayList<Integer> dateArrayList = new ArrayList<>();
        if (id !=0){
            for (Map.Entry<Integer, Integer> entry : dateArray.entrySet()){
                if (entry.getKey() == id){
                    dateArrayList.add(entry.getValue());
                }
            }
        }
        return dateArrayList;
    }

    private String getName(int id){
        String name = "";
        for (Map.Entry<Integer, String> entry: nameMap.entrySet()){
            if (entry.getKey() == id){
                name = entry.getValue();
            }
        }

        return name;
    }


    public void add(PatientRecord pr) {
        patientRecords.add(new PatientRecord(pr.getId(), pr.getName(), pr.getReasonForVisit(), pr.getTreatment(), pr.getDate()));
        reasonMap.put(pr.getId(), pr.getReasonForVisit());
        treatmentMap.put(pr.getId(), pr.getTreatment());
        dateArray.put(pr.getId(), pr.getDate());
        nameMap.put(pr.getId(),pr.getName());

    }

    public String printPatient(int id) {
        String print = (getName(id) + " " + getDates(id) + " " + getReason(id) + " " + getTreatment(id) + " " + id);
        return print;
    }
}




