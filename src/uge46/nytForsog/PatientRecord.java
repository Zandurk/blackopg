package uge46.nytForsog;

/**
 * Created by Alex on 04-12-2015.
 */
public class PatientRecord extends PatientDatabase {


    String navn;
    String reason;
    String treatment;
    String date;

    public PatientRecord(String navn, String reason, String treatment, String date) {
        this.navn = navn;
        this.reason = reason;
        this.treatment = treatment;
        this.date = date;
    }


    public String getNavn() {
        return navn;
    }

    public String getReason() {
        return reason;
    }

    public String getTreatment() {
        return treatment;
    }

    public String getDate() {
        return date;
    }
}
