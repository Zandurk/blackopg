package uge46.nytForsog;

/**
 * Created by Alex on 04-12-2015.
 */
public class main {
/*
2. Consider records for patients at a medical facility. Each record contains an integer identification for a patient and
    strings for the date, the reason for the visit, and the treatment prescribed. Design and implement the class
    PatientRecord so that it overrides the method hashCode. Write a main program that tests your new class.

3. Design a class PatientDataBase that stores instances of PatientRecord, as described in the previous project. The
  class should provide at least three query operations, as follows. Given a patient identification and date, the first
  operation should return the reason for the visit, and the second operation should return the treatment. The third
  query operation should return a list of dates, given a patient identification.

  */
    public static void main(String[] args) {
        PatientRecord pr = new PatientRecord("John", "Aids", "Died", "2108");
        PatientDatabase pt = new PatientDatabase();

        pt.addPatient("John", "Aids", "Died", "2108");
        pt.addPatient("Johns","Tuberkulose", "Antibiotika", "0910");
        System.out.println(pt.getName("Aids"));
        System.out.println(pt.getDate("John"));
        System.out.println(pt.getReason("John"));
        System.out.println(pt.getTreatment("Johns"));
    }


}
