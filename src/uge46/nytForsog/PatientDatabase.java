package uge46.nytForsog;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 04-12-2015.
 */
public class PatientDatabase {
    ArrayList<PatientRecord> patientRecords = new ArrayList<>();
    HashMap<String, String> nameMap = new HashMap<>();
    HashMap<Integer, String> dateMap = new HashMap<>();
    HashMap<Integer, String> reasonMap = new HashMap<>();
    HashMap<Integer, String> treatmentMap = new HashMap<>();


    public void addPatient(String navn, String reason, String treament, String date){
        patientRecords.add(new PatientRecord(navn,reason, treament, date));
        nameMap.put(reason, navn);
        dateMap.put(getId(navn), date);
        reasonMap.put(getId(navn), reason);
        treatmentMap.put(getId(navn), treament);

    }

    public String getName(String reason){
        String navn = "";
       if (reason != null) {
           for (Map.Entry<String , String> entry : nameMap.entrySet()) {
               if (entry.getKey() == reason){
                   navn = entry.getValue();
               }

           }
       }
        return navn;
    }


    public String getDate(String navn){
        if (navn != null) {
            int i = hashCode(navn);
            for (Map.Entry<Integer , String> entry : dateMap.entrySet()) {
                if (entry.getKey() == i){
                    navn = entry.getValue();
                }

            }
        }
        return navn;
    }

    public String getReason(String navn) {
        if (navn != null) {
            int i = hashCode(navn);
            for (Map.Entry<Integer , String> entry : reasonMap.entrySet()) {
                if (entry.getKey() == i){
                    navn = entry.getValue();
                }

            }
        }
        return navn;
    }

    public String getTreatment(String navn) {
        if (navn != null) {
            int i = hashCode(navn);
            for (Map.Entry<Integer , String> entry : treatmentMap.entrySet()) {
                if (entry.getKey() == i){
                    navn = entry.getValue();
                }

            }
        }
        return navn;
    }
    public int getId(String s){
        return hashCode(s);
    }

    private int hashCode(String s){
        int hash=0;
        int g=31;
        for (int i = 0; i <s.length() ; i++)
        {
            hash = hash*g + s.charAt(i);
        }
        return hash;
    }


}
