package uge39;

/**
 * Created by Alexander on 06-11-2015.
 */
public class main {
    public static void main(String[] args) {
        //12.2
        abetterexampleofrecursion();

        //12.3
        recursivefunctionsanddata();
        //mystery4
        System.out.println(mystery4(6, 13));
        System.out.println(mystery4(14, 10));
        System.out.println(mystery4(37, 10));
        System.out.println(mystery4(8, 2));
        System.out.println(mystery4(50, 7));
        //mystery5
        System.out.println(mystery5(5,7));
        System.out.println(mystery5(12,9));
        System.out.println(mystery5(-7,4));
        System.out.println(mystery5(-23,-48));
        System.out.println(mystery5(128,343));
        //mystery6
        System.out.println(mystery6(7,1));
        System.out.println(mystery6(4,2));
        System.out.println(mystery6(4,3));
        System.out.println(mystery6(5,3));
        System.out.println(mystery6(5,4));
        /*
        Kap 12
        Exec 2
         */
        System.out.println(writeSums(2));

        /*
        *  Kap 13
            Exec 20:
          */
     //   arrayshuffle();
        /*
         Exec 8
        number1 = 9, 18, 27, 36, 45, 54, 63, 72;
        number2 = 12, 19, 23, 29, 37, 48, 55, 74;

        Exec 9
        9, 63
        9, 45, 63, 72,
        9, 45, 63, 27, 72
        9, 27, 45, 63, 18, 72
        9, 27, 18, 45, 54, 63, 72, 36
        9, 18, 27, 45, 54, 36, 63, 72

       Exec 10
       numbers 3 = -9, -7, -1, 0, 3, 5, 8, 14;
       numbers 4 = -4, 5, 10, 15, 24, 27, 39, 56;
         */



    }

    private static int writeSums(int i) {
        int n = i;
        if(i == 0){
            return 0;
        } else if(i >= n) {
            return writeSums(i+1);
        }else{
            return i;
        }
    }

    private static void arrayshuffle() {
        int[] ina = new int[10];
        int[] inb = ina.clone();
        for(int i = 0; i < ina.length; i++){
            for (int j = 0; j >= i; j++){
                ina[j] = inb[i];
                ina[i] = inb[j];
            }

        }
    }

    private static void recursivefunctionsanddata() {
        //11.
        /*
        * public static int pow(int x, int y){
        *   return x * pow(x, y - 1);
        * }
        *  Problemet med denne metode er, at den aldrig stopper, der er ikke en selektion, der beder den om at stoppe.
        *
        * 12. Jeg kan ikke se to metoder, men g�r udfra at den ene har en if-statement.
        *
        * 13.
        */
    }
        public static int mystery4(int x, int y){
            if(x < y){
                return x;
            } else {
                return mystery4(x-y, y);
            }

        }
    /*
    14.
     */
    public static int mystery5(int x,int y){
        if (x < 0 ){
            return -mystery5(-x,y);
        } else if (y < 0){
            return -mystery5(x,-y);
        }else if (x == 0 && y == 0){
            return 0;
        } else{
            return 100 * mystery5(x/10, y/10) + 10*(x%10) + y % 10;
        }
    }
    /*
    15.
     */
    public static int mystery6(int x, int y){
        if(x == 0 || x == y){
            return 1;
        } else if (x > y){
            return 0;
        } else{
            return mystery6(y-1,x-1) + mystery6(y-1, x);
        }
    }
    /*
    16.
     */
    public static int factorial(int x){
        int y = 1;
        if(x == y){
            return x;
        } else{
            return x *= y+1;
        }
    }
    /*
    18.
    Klienten beh�ver ikke at vide hvad vi g�r med parameterne, derfor kan vi bare ignorere dem.


    19.
     */


    private static void abetterexampleofrecursion() {
        /*
          8:
        * Call stack har noget at g�re med den m�de kaldene i en metode eller et program fungere, f.eks. skal man altid
        * tage hensyn til hvordan man kalder en metode der printer flere dele af en String,
        * da den ellers kan printe det i forkert r�kkef�lge. - Dette er meget relevant n�r vi snakker om selektioner.
        *
        *
        * 9.
        * Metoden her tager i mod et Scanner-objekt.
        * public static void reverse(Scanner input){
        *    - Tjekker om der er en linje mere.
        *   if(input.hasNextLine()){
        *   - Gemmer linjen i String
        *   String line = input.hasNextLine();
        *   - Printer linjen ud.
        *   System.out.println(line);
        *   - Kalder metoden igen, s� den k�rer i et "loop" indtil der ikke er flere linjer.
        *   reverse(input);
        *   }
        * }
        *
        * 10.
        * public static void reverse(Scanner input){
        *    - Tjekker om der er en linje mere.
        *   if(input.hasNextLine()){
        *   - Kalder metoden igen, s� den k�rer i et "loop" indtil der ikke er flere linjer.
        *   reverse(input);
         *  - Gemmer linjen i String
        *   String line = input.hasNextLine();
        *   - Printer linjen ud.
        *   System.out.println(line);
        *
        *       - Problemet her er, at den aldrig, f�r den sidste linje, n�r ned til at printe Stringen ud.
        *
        *   }
        * }
        *
         * */
    }

}
