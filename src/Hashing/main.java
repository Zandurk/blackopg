package Hashing;

/**
 * Created by Alexander on 13-11-2015.
 */
public class main {
    public static void main(String[] args) {
        /*
        Consider records for patients at a medical facility. Each record contains an integer identification for a patient and
        strings for the date, the reason for the visit, and the treatment prescribed. Design and implement the class
                PatientRecord so that it overrides the method hashCode. Write a main program that tests your new class.*/

        PatientRecord pr = new PatientRecord();
        System.out.println(pr.hashCode());
    }

}
