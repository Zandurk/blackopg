/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uge38;

import java.sql.Date;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author MiaSmith
 */
class Member {
      private ArrayList<Member> members;
    private ArrayList<Membership> memberships;
    private ArrayList<Zip> zips;
    
    private String socialSecNum;
    private String firstName;
    private String lastName;
    private String address;
    private int phone;
    private String mail;
    private int zip;
    private int memID;
    private String desc;
    private String city;
    private Date joinDate;
    private Date expiration;
    private int klip;
    private long days;
    private int memShipID;

 
    protected int getMemShipID() {
        return memShipID;
    }

    protected void setMemShipID(int memShipID) {
        this.memShipID = memShipID;
    }

    protected long getDays() {
        return days;
    }

    protected void setDays(long days) {
        this.days = days;
    }

    protected void setKlip(int klip) {
        this.klip = klip;
    }
    
    protected int getKlip() {
        return klip;
    }

    protected void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    protected Date getJoinDate() {
        return joinDate;
    }

    protected void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    protected Date getExpiration() {
        return expiration;
    }

    protected int getmemID() {
        return memID;
    }

    protected int getzip() {
        return zip;
    }

    protected String getsocialSecNum() {
        return socialSecNum;
    }

    protected String getfirstName() {
        return firstName;
    }

    protected String getlastName() {
        return lastName;
    }

    protected String getaddress() {
        return address;
    }

    protected int getphone() {
        return phone;
    }

    protected String getmail() {
        return mail;
    }

    protected String getdesc() {
        return desc;
    }

    protected String getcity() {
        return city;
    }

    protected void setmemID(int memID) {
        this.memID = memID;
    }

    protected void setzip(int zip) {
        this.zip = zip;
    }

    protected void setsocialSecNum(String socialSecurityNum) {
        this.socialSecNum = socialSecurityNum;
    }

    protected void setfirstName(String FName) {
        this.firstName = FName;
    }

    protected void setlastName(String LName) {
        this.lastName = LName;
    }

    protected void setaddress(String adress) {
        this.address = adress;
    }

    protected void setphone(int phone) {
        this.phone = phone;
    }

    protected void setmail(String mail) {
        this.mail = mail;
    }

    protected void setdesc(String desc) {
        this.desc = desc;
    }

    protected void setcity(String city) {
        this.city = city;
    }
    
    boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
//  [\\w\\.-]+: Begins with word characters, 
//  @: It must have a '@' symbol after initial characters.
//  ([\\w\\-]+\\.)+: '@' must follow by more alphanumeric characters. This part must also have a "." to separate domain and subdomain names. 
//  [A-Z]{2,4}$ : Must end with two to four alaphabets. (This will allow domain names with 2, 3 and 4 characters e.g. dk, com, net

        CharSequence inputStr = email;

        //Make the comparison case-insensitive.  
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    
    boolean isCPRcorrect(String SSNString){
      if (SSNString.charAt(6) == '-' && SSNString.length() == 11) {
        return true;
    }
      return false;
    }
      
    boolean containsZip(int zipsearch) {
        if (zips.stream().anyMatch((zips) -> (zips.getZipcode() == zipsearch))) {
            return true;
        }
        return false;
    }


}

    

