package uge38;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
*/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.text.StyleConstants.ALIGN_CENTER;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Alex
 */
public class FileMember {

    //Forbindelse med DB
    private String os = System.getProperty("os.name").toLowerCase();

    String path;
    String userHome;
    String fname;
    String lname;
    String fname1;
    String lname1;
    int id;
    String cpr;
    String address;
    int zip;
    String city;
    String mail;
    int phone;
    int docId;
    double memShip;
    int memShipId;
    String memDuration;
    String memDesc;
    Date start;
    Date exp;


    FileMember(int memID) {
        //Instans

        ArrayList<Member> members = new ArrayList<>();
        ArrayList<Membership> memberships = new ArrayList<>();
        for (Member mem : members) {
            //hvis member id stemmer overens
            if (mem.getmemID() == memID) {
                fname1 = mem.getfirstName();
                lname1 = mem.getlastName();
                id = mem.getmemID();
                cpr = mem.getsocialSecNum();
                address = mem.getaddress();
                zip = mem.getzip();
                city = mem.getcity();
                mail = mem.getmail();
                phone = mem.getphone();
                memShipId = mem.getMemShipID();
                start = mem.getJoinDate();
                exp = mem.getExpiration();
            }
            //Henter membership
            for (Membership mems : memberships) {
                //Membershipid skal stemme overens
                if (memShipId == mems.getMembershipID()) {
                    memShip = mems.getPrice();
                    memDuration = mems.getDuration();
                    memDesc = mems.getDesc();
                }

            }
        }/*
        memberPDF(fname1, lname1, id, cpr, address, zip, city, mail, phone, memShip, memShipId, memDuration, memDesc, start, exp);

    }
/*
    //Laver PDF via IText-library
    private void memberPDF(String fname, String lname, int id, String cpr, String address, int zip, String city, String mail, int phone, double memShipPrice, int memShipId, String memDuration, String memDesc, Date start, Date exp) {

        Document doc = new Document();

        try {
            File file = new File(fname + lname + ".pdf");

            String userHome = System.getProperty("user.name");
          
            //Tjekker på os og lægger fil på skrivebordet
            if(os.equals("win")){
            String path = "C:\\" + "\\Users\\" + userHome + "\\Desktop\\" + fname + lname + ".pdf";
            }else if(os.equals("mac")){
            String path = "/Users/MiaSmith/Desktop/" + fname + lname + ".pdf";
            }
            
            PdfWriter.getInstance(doc, new FileOutputStream(path));
            
            
            String idStr = String.valueOf(id);
            String zipStr = String.valueOf(zip);
            String phoneStr = String.valueOf(phone);
            String memship = String.valueOf(memShip);
            Calendar calendar = Calendar.getInstance();
            Date today = new Date(calendar.getTime().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String todayString = formatter.format(today);
            String startDate = formatter.format(start);
            String endDate = formatter.format(exp);

            //Det skal se pænt ud
            Font font1 = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
      
            //Document - bruger IText-API til at sætte PDFen op.
            doc.open();
            Paragraph pTitel = new Paragraph("Faktura til medlem: #" + idStr + " " + fname.toUpperCase() + " " + lname.toUpperCase(), font1);
            pTitel.setAlignment(ALIGN_CENTER);

            Paragraph dateParagraph = new Paragraph("Faktura er oprettet d. " + todayString);

            Paragraph lifenfitParagraph = new Paragraph("Life n Fit");
            Paragraph addressParagraph = new Paragraph("Engvangen 13");
            Paragraph zipParagraph = new Paragraph("2680 Solrød");

            Paragraph memIDParagraph = new Paragraph("Medlems-ID: " + idStr);
            Paragraph nameParagraph = new Paragraph("Navn: " + fname + lname);
            Paragraph SSNParagraph = new Paragraph("CPR: " + cpr);
            Paragraph memaddressParagraph = new Paragraph("Adresse: " + address);
            Paragraph phoneParagraph = new Paragraph("Telefon: " + phoneStr);
            Paragraph mailParagraph = new Paragraph("Mail: " + mail);
            
            Paragraph memShipParagraph = new Paragraph("Medlemskab:");
            Paragraph memShipInfoParagraph = new Paragraph(memDesc + "                                   " + memDuration + "                                 kr. " + memShipPrice);
                
            Paragraph startParagraph = new Paragraph("Startdato: " + startDate);
            Paragraph expParagraph = new Paragraph("Udløbsdato: " + endDate);
            
            Paragraph paidParagraph = new Paragraph("Betalt d. _______________");
            
            Paragraph signatureParagraph = new Paragraph("________________________________________");
            Paragraph employeeParagraph = new Paragraph("              (underskrift medarbejder)");

            doc.add(pTitel);
            doc.add(new Paragraph(" "));
            doc.add(dateParagraph);
            doc.add(new Paragraph(" "));
            doc.add(lifenfitParagraph);
            doc.add(addressParagraph);
            doc.add(zipParagraph);
            doc.add(new Paragraph(" "));
            doc.add(memIDParagraph);
            doc.add(nameParagraph);
            doc.add(SSNParagraph);
            doc.add(memaddressParagraph);
            doc.add(phoneParagraph);
            doc.add(mailParagraph);
            doc.add(new Paragraph(" "));
            doc.add(memShipParagraph);
            doc.add(memShipInfoParagraph);
            doc.add(new Paragraph(" "));
            doc.add(startParagraph);
            doc.add(expParagraph);
            doc.add(new Paragraph(" "));
            doc.add(new Paragraph(" "));
            doc.add(paidParagraph);
            doc.add(new Paragraph(" "));
            doc.add(signatureParagraph);
            doc.add(employeeParagraph);
            
           
            doc.newPage();
            doc.close();

        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(FileMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        //PDF oprettet
        JOptionPane.showMessageDialog(null, "PDF oprettet! Den ligger på skrivebordet!");

    }


    /*

     private void launchFile(String fname, String lname, String path, String userHome){

     //#TODO
     }  

}
*/
    }
}