/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uge38;

/**
 *
 * @author MiaSmith
 */
class Membership {
    private int membershipID;
    private String desc;
    private double price;
    private String duration;

    public int getMembershipID() {
        return membershipID;
    }

    public String getDesc() {
        return desc;
    }

    public double getPrice() {
        return price;
    }

    public String getDuration() {
        return duration;
    }

    public void setMembershipID(int membershipID) {
        this.membershipID = membershipID;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

   
    
    
}
